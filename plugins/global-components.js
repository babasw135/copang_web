import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import cardCopangInfo from '~/components/card-copang-info'
Vue.component('cardCopangInfo', cardCopangInfo)

import rorChInfo from '~/components/ror-ch-info'
Vue.component('rorChInfo', rorChInfo)
